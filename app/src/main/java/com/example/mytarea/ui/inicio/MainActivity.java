package com.example.mytarea.ui.inicio;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.mytarea.common.Constantes;
import com.example.mytarea.R;
import com.example.mytarea.common.SharedPreferencesManager;
import com.example.mytarea.data.UsuarioViewModel;
import com.example.mytarea.common.Utility;
import com.example.mytarea.databinding.ActivityMainBinding;
import com.example.mytarea.OnListeners;
import com.example.mytarea.db.Entity.UsuarioEntity;
import com.example.mytarea.ui.TareasActivity;

public class MainActivity extends AppCompatActivity {

    private UsuarioViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String username=SharedPreferencesManager.getSomeStringValue(Constantes.PREF_USER);
        if(username != null && !username.isEmpty()){
            Intent i = new Intent(MainActivity.this, TareasActivity.class);
            startActivity(i);
            finish();
        }
        final ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        viewModel= ViewModelProviders.of(MainActivity.this).get(UsuarioViewModel.class);
        binding.setHandler(new OnListeners() {
            @Override
            public void onLoginEvent(View v) {

                String email = binding.etEmail.getText().toString();
                String pass = binding.etPassword.getText().toString();
               if(email.isEmpty()){
                   binding.etEmail.setError(getString(R.string.error_correo));
               }else if(pass.isEmpty()){
                   binding.etPassword.setError(getString(R.string.erro_pass));
               }else if(!Utility.validarEmail(email)){
                   binding.etEmail.setError(getString(R.string.error_no_es_correo));
               }else{

                  viewModel.LoginUsuario(email,pass).observe(MainActivity.this, new Observer<UsuarioEntity>() {
                      @Override
                      public void onChanged(UsuarioEntity usuarioEntity) {
                          if(usuarioEntity==null){
                              Toast.makeText(MainActivity.this,getString(R.string.error_login),Toast.LENGTH_LONG).show();
                          }else{
                              SharedPreferencesManager.setSomeIntValue(Constantes.PREF_ID_USER,usuarioEntity.id);
                              SharedPreferencesManager.setSomeStringValue(Constantes.PREF_USER,usuarioEntity.userName);
                              Intent i = new Intent(MainActivity.this, TareasActivity.class);
                              startActivity(i);
                              finish();
                          }
                      }
                  });

               }
            }

            @Override
            public void onSignUp(View v) {
                Intent i =new Intent(MainActivity.this,SignUpActivity.class);
                startActivity(i);
                finish();
            }
        });
    }
}
