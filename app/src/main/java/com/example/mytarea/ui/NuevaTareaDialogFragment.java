package com.example.mytarea.ui;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.mytarea.NuevaTareaOnListeners;
import com.example.mytarea.R;
import com.example.mytarea.common.Constantes;
import com.example.mytarea.common.SharedPreferencesManager;
import com.example.mytarea.common.Utility;
import com.example.mytarea.data.TareasViewModel;
import com.example.mytarea.databinding.NuevaTareaFullDialogBinding;
import com.example.mytarea.db.Entity.TareaEntity;

import java.util.Date;

public class NuevaTareaDialogFragment extends DialogFragment {
    int idUsuario;
    private NuevaTareaFullDialogBinding binding;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = NuevaTareaFullDialogBinding.inflate(inflater,container,false);
        View view=binding.getRoot();

        binding.setHandler(new NuevaTareaOnListeners() {
            @Override
            public void onGuardar(View v) {
                Guardar();
            }

            @Override
            public void onClose(View v) {
                if(!binding.etTitulo.getText().toString().isEmpty() || !binding.etTarea.getText().toString().isEmpty()) {
                    showDialogConfirm().show();
                }else{
                    getDialog().dismiss();
                }

            }
        });

        idUsuario= SharedPreferencesManager.getSomeIntValue(Constantes.PREF_ID_USER);
        return view;
    }




    private void Guardar() {
        String titulo=binding.etTitulo.getText().toString();
        String tarea=binding.etTarea.getText().toString();

        if(titulo.isEmpty()){
            binding.etTitulo.setError("El campo de titulo es obligatorio.");
        }else if(tarea.isEmpty()) {
            binding.etTarea.setError("El campo de tarea es obligatorio.");
        }else{
            TareasViewModel tweetViewModel = new ViewModelProvider(getActivity()).get(TareasViewModel.class);
            Date fecha = new Date();
            TareaEntity tareaEntity =new TareaEntity(idUsuario,titulo, Utility.formatearFecha(fecha),tarea);
            tweetViewModel.crearTarea(tareaEntity);
            getDialog().dismiss();
        }
    }

    private AlertDialog showDialogConfirm(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(R.string.dialog_message)
                .setTitle(R.string.dialog_title);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                getDialog().dismiss();

            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();

        return dialog;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding=null;
    }
}
