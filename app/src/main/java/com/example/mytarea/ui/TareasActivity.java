package com.example.mytarea.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.example.mytarea.R;
import com.example.mytarea.common.Constantes;
import com.example.mytarea.common.SharedPreferencesManager;
import com.example.mytarea.databinding.ActivityTareasBinding;
import com.example.mytarea.ui.inicio.MainActivity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class TareasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ActivityTareasBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_tareas);
        setSupportActionBar(binding.toolbar);


        binding.fab.setOnClickListener(view->{
            NuevaTareaDialogFragment dialogFragment=new NuevaTareaDialogFragment();
            dialogFragment.show(getSupportFragmentManager(),"NuevaTareaDialogFragment");
        });
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.nav_host_fragment, TareasFragment.newInstance())
                .commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater= getMenuInflater();
        inflater.inflate(R.menu.menu_tarea,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==R.id.id_menu_logout){
            DialogoLogout();
        }
        return true;
    }

    void DialogoLogout(){

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(getString(R.string.confirmacion_cerrar_sesion))
                .setPositiveButton(R.string.si, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        SharedPreferencesManager.setSomeIntValue(Constantes.PREF_ID_USER,-1);
                        SharedPreferencesManager.setSomeStringValue(Constantes.PREF_USER,"");
                        Intent intent = new Intent(TareasActivity.this, MainActivity.class);
                        TareasActivity.this.startActivity(intent);
                        dialog.dismiss();
                        finish();

                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        dialog.create().show();

    }
}
