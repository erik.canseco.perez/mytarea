package com.example.mytarea.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;

import com.example.mytarea.R;
import com.example.mytarea.common.Constantes;
import com.example.mytarea.common.SharedPreferencesManager;
import com.example.mytarea.data.TareasViewModel;
import com.example.mytarea.databinding.ButtomModalTareaFragmentBinding;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.navigation.NavigationView;

public class ButtomModalTareaFragment extends BottomSheetDialogFragment {
    private TareasViewModel tareaViewModel;
    private int idTarea;
    private int idUsuario;
    private ButtomModalTareaFragmentBinding binding;

    public static ButtomModalTareaFragment newInstance(int idTarea) {
        ButtomModalTareaFragment fragment = new ButtomModalTareaFragment();
        Bundle args = new Bundle();
        args.putInt(Constantes.ARG_TAREA_ID,idTarea);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null)
            idTarea =  getArguments().getInt(Constantes.ARG_TAREA_ID,-1);
        idUsuario= SharedPreferencesManager.getSomeIntValue(Constantes.PREF_ID_USER);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding=ButtomModalTareaFragmentBinding.inflate(inflater,container,false);
        View v = binding.getRoot();

        binding.navigationViewBottomTweet.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId())
                {
                    case R.id.action_delete_tarea:
                        if(idTarea>-1) {
                            tareaViewModel.deleteTarea(idTarea,idUsuario);
                        }
                        getDialog().dismiss();
                        break;
                }
                return false;
            }
        });

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tareaViewModel = ViewModelProviders.of(getActivity()).get(TareasViewModel.class);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding=null;
    }
}
