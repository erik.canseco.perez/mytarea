package com.example.mytarea.ui;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mytarea.R;
import com.example.mytarea.common.Constantes;
import com.example.mytarea.common.SharedPreferencesManager;
import com.example.mytarea.data.TareasViewModel;
import com.example.mytarea.databinding.FragmentTareasListBinding;
import com.example.mytarea.db.Entity.TareaEntity;


import java.util.ArrayList;
import java.util.List;


public class TareasFragment extends Fragment {


    private RecyclerView recyclerView;
    private MyTareasRecyclerViewAdapter adapter;
    private TareasViewModel tareasViewModel;
    private int idUsuario;
    private List<TareaEntity> listTareas;
    private FragmentTareasListBinding binding;

    public TareasFragment() {
    }

    public static TareasFragment newInstance() {
        TareasFragment fragment = new TareasFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tareasViewModel= ViewModelProviders.of(getActivity()).get(TareasViewModel.class);
        idUsuario= SharedPreferencesManager.getSomeIntValue(Constantes.PREF_ID_USER);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding=FragmentTareasListBinding.inflate(inflater,container,false);

        View view = binding.getRoot();

        binding.list.setLayoutManager(new LinearLayoutManager(view.getContext()));
        listTareas = new ArrayList<TareaEntity>();
        adapter=new MyTareasRecyclerViewAdapter(listTareas,getActivity());
        binding.list.setAdapter(adapter);
        loadTareaData();

        return view;
    }

    private void loadTareaData(){
        tareasViewModel.getTareasUsuario(idUsuario).observe(getActivity(), new Observer<List<TareaEntity>>() {
            @Override
            public void onChanged(List<TareaEntity> tareaEntities) {
                listTareas=tareaEntities;
                adapter.setNuevasTarea(tareaEntities);
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding=null;
    }
}
