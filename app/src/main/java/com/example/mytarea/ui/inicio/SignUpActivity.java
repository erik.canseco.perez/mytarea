package com.example.mytarea.ui.inicio;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.mytarea.common.Constantes;
import com.example.mytarea.R;
import com.example.mytarea.RegistroOnListeners;
import com.example.mytarea.common.SharedPreferencesManager;
import com.example.mytarea.data.UsuarioViewModel;
import com.example.mytarea.common.Utility;
import com.example.mytarea.databinding.ActivitySignUpBinding;
import com.example.mytarea.db.Entity.UsuarioEntity;
import com.example.mytarea.ui.TareasActivity;

public class SignUpActivity extends AppCompatActivity {

    private UsuarioViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ActivitySignUpBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        viewModel= ViewModelProviders.of(SignUpActivity.this).get(UsuarioViewModel.class);
        binding.setHandler(new RegistroOnListeners() {
            @Override
            public void onLoginEvent(View v) {
                Intent i =new Intent(SignUpActivity.this,MainActivity.class);
                startActivity(i);
                finish();
            }

            @Override
            public void onSignUp(View v) {
                String userName=binding.etUserName.getText().toString();
                String email = binding.etEmail.getText().toString();
                String pass = binding.etPassword.getText().toString();
                if(userName.isEmpty()) {
                    binding.etUserName.setError(getString(R.string.error_username));
                }else if(email.isEmpty()){
                    binding.etEmail.setError(getString(R.string.error_correo));
                }else if(pass.isEmpty()){
                    binding.etPassword.setError(getString(R.string.erro_pass));
                }else if(!Utility.validarEmail(email)){
                    binding.etEmail.setError(getString(R.string.error_no_es_correo));
                }else if(pass.length()<4){
                    binding.etPassword.setError(getString(R.string.error_num_pass));
                }else{
                    viewModel.ExisteUsuario(email).observe(SignUpActivity.this, new Observer<UsuarioEntity>() {
                        @Override
                        public void onChanged(UsuarioEntity usuarioEntity) {
                            if(usuarioEntity == null){
                                registro(userName,email,pass);

                            }else{
                                Toast.makeText(SignUpActivity.this,getString(R.string.error_existe),Toast.LENGTH_LONG).show();
                            }
                            viewModel.ExisteUsuario(email).removeObserver(this);
                        }
                    });

                }

            }
        });
    }

    private void registro(String userName,String email,String pass){
        viewModel.RegistroUsuario(new UsuarioEntity(userName,email,pass));
        viewModel.LoginUsuario(email, pass).observe(SignUpActivity.this, new Observer<UsuarioEntity>() {
            @Override
            public void onChanged(UsuarioEntity usuarioEntity) {
                if(usuarioEntity != null){
                    SharedPreferencesManager.setSomeIntValue(Constantes.PREF_ID_USER,usuarioEntity.id);
                    SharedPreferencesManager.setSomeStringValue(Constantes.PREF_USER,usuarioEntity.userName);
                    Intent i = new Intent(SignUpActivity.this, TareasActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        });

    }
}
