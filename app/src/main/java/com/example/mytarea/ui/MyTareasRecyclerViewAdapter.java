package com.example.mytarea.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;

import android.view.ViewGroup;

import com.example.mytarea.data.TareasViewModel;
import com.example.mytarea.databinding.FragmentTareasBinding;
import com.example.mytarea.db.Entity.TareaEntity;

import java.util.List;

public class MyTareasRecyclerViewAdapter extends RecyclerView.Adapter<MyTareasRecyclerViewAdapter.ViewHolder> {

    private List<TareaEntity> mValues;
    private  Context context;
    private TareasViewModel mViewModel;
    private FragmentTareasBinding binding;


    public MyTareasRecyclerViewAdapter(List<TareaEntity> items, Context context) {
        mValues = items;
        this.context=context;
        mViewModel= ViewModelProviders.of((AppCompatActivity)context).get(TareasViewModel.class);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        binding =FragmentTareasBinding.inflate(LayoutInflater.from(parent.getContext()),parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.binding.tvTitulo.setText(holder.mItem.titulo);
        holder.binding.tvFecha.setText(holder.mItem.fecha);
        holder.binding.tvTarea.setText(holder.mItem.tarea);

        holder.binding.ivMenu.setOnClickListener(view->{
            mViewModel.openDialodTweetMenu(context,holder.mItem.id);
         });
    }

    public void setNuevasTarea(List<TareaEntity> tareaEntities){
        this.mValues=tareaEntities;
        notifyDataSetChanged();
    }
    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public FragmentTareasBinding binding;
        public TareaEntity mItem;

        public ViewHolder(FragmentTareasBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
