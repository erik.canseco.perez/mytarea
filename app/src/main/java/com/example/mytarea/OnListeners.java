package com.example.mytarea;

import android.view.View;

public interface OnListeners {

    void onLoginEvent(View v);

    void onSignUp(View v);

}
