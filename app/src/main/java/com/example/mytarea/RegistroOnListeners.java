package com.example.mytarea;

import android.view.View;

public interface RegistroOnListeners {
    void onLoginEvent(View v);

    void onSignUp(View v);
}
