package com.example.mytarea;

import android.view.View;

public interface NuevaTareaOnListeners {
    void onGuardar(View v);

    void onClose(View v);
}
