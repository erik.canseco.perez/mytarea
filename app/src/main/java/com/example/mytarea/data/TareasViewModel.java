package com.example.mytarea.data;

import android.app.Application;
import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.mytarea.ui.ButtomModalTareaFragment;
import com.example.mytarea.db.Entity.TareaEntity;

import java.util.List;

public class TareasViewModel  extends AndroidViewModel {
    private TareasRepository tareasRepository;

    public TareasViewModel( Application application) {
        super(application);
        tareasRepository = new TareasRepository(application);
    }

    public LiveData<List<TareaEntity>> getTareasUsuario(int idUsuario){
        return tareasRepository.getAllTareasUser(idUsuario);
    }

    public void crearTarea(TareaEntity tarea){
        tareasRepository.crearTarea(tarea);
    }

    public void openDialodTweetMenu(Context context, int idUsuario){
        ButtomModalTareaFragment modalTareaFragment =
                ButtomModalTareaFragment.newInstance(idUsuario);
        modalTareaFragment.show(((AppCompatActivity)context).getSupportFragmentManager(),"ButtomModalTareaFragment");
    }

    public void deleteTarea(int idTarea,int idUsuario){
        tareasRepository.deleteTarea(idTarea,idUsuario);
    }
}
