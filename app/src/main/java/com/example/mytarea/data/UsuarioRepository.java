package com.example.mytarea.data;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;


import com.example.mytarea.db.Entity.UsuarioEntity;
import com.example.mytarea.db.TareaRoomDatabase;
import com.example.mytarea.db.dao.TareaDao;


public class UsuarioRepository {

    private TareaDao tareaDao;

    public UsuarioRepository(Application application) {
        TareaRoomDatabase db=TareaRoomDatabase.getDatabase(application);
        tareaDao=db.tareaDao();
    }

    public LiveData<UsuarioEntity> LoginUsuario(String email, String pass){
        LiveData<UsuarioEntity> usuario=null;
        //int existe = tareaDao.existeUsuario(email,pass);
        //if(existe>0){
            usuario=tareaDao.buscaUsuario(email,pass);
        //}
        return usuario;
    }

    public void Registro(final UsuarioEntity usuarioEntity){
        TareaRoomDatabase.databaseWriteExecutor.execute(()->{
              tareaDao.insert(usuarioEntity);
                });
    }

    public LiveData<UsuarioEntity> BuscaUsuario(String email) {
        LiveData<UsuarioEntity> usuario=null;

        usuario=tareaDao.existeUsuario(email);

        return usuario;
    }


}
