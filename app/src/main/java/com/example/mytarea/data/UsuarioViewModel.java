package com.example.mytarea.data;

import android.app.Application;


import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.mytarea.db.Entity.UsuarioEntity;

public class UsuarioViewModel  extends AndroidViewModel {
    private UsuarioRepository usuarioRepository;

    public UsuarioViewModel(Application application) {
        super(application);
        usuarioRepository = new UsuarioRepository(application);
    }

    public LiveData<UsuarioEntity> LoginUsuario(String email, String pass){
        return usuarioRepository.LoginUsuario(email,pass);

    }

    public void RegistroUsuario(UsuarioEntity usuarioEntity){
         usuarioRepository.Registro(usuarioEntity);
    }
    public LiveData<UsuarioEntity> ExisteUsuario(String email){
        return usuarioRepository.BuscaUsuario(email);

    }
}
