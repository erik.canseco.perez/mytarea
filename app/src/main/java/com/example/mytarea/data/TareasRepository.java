package com.example.mytarea.data;

import android.app.Application;

import androidx.lifecycle.LiveData;

import com.example.mytarea.db.Entity.TareaEntity;
import com.example.mytarea.db.TareaRoomDatabase;
import com.example.mytarea.db.dao.TareaDao;

import java.util.List;

public class TareasRepository {

    private TareaDao tareaDao;
    private LiveData<List<TareaEntity>> allTareas;

    public TareasRepository(Application application) {
        TareaRoomDatabase db=TareaRoomDatabase.getDatabase(application);
        tareaDao=db.tareaDao();
    }

    public LiveData<List<TareaEntity>> getAllTareasUser(int idUsuario){
        allTareas=tareaDao.getTarea(idUsuario);
        return allTareas;
    }

    public void crearTarea(TareaEntity tarea){
        TareaRoomDatabase.databaseWriteExecutor.execute(()->{
            tareaDao.insertTarea(tarea);
            allTareas=tareaDao.getTarea(tarea.idUsuario);
        });
    }

    public void deleteTarea(int idTarea,int idUsuario){
        TareaRoomDatabase.databaseWriteExecutor.execute(()->{
            tareaDao.deleteTarea(idTarea);
            allTareas=tareaDao.getTarea(idUsuario);
        });
    }
}