package com.example.mytarea.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.mytarea.db.Entity.TareaEntity;
import com.example.mytarea.db.Entity.UsuarioEntity;

import java.util.List;

@Dao
public interface TareaDao {

    @Query("Select * from usuarios where email= :datoEmail")
    LiveData<UsuarioEntity> existeUsuario(String datoEmail);

    @Query("Select * from usuarios where email= :datoEmail and password= :datoPass")
     LiveData<UsuarioEntity> buscaUsuario(String datoEmail, String datoPass);

    @Insert
    void insert(UsuarioEntity usuario);

    @Query("Select * from tarea where idUsuario = :idUsuario")
    LiveData<List<TareaEntity>> getTarea(int idUsuario);

    @Insert
    void insertTarea(TareaEntity tarea);

    @Query("DELETE FROM tarea where id = :idTarea")
    void deleteTarea(int idTarea);






}
