package com.example.mytarea.db.Entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.sql.Date;

@Entity(tableName = "tarea")
public class TareaEntity {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "idUsuario")
    public int idUsuario;
    @ColumnInfo(name = "titulo")
    public String titulo;
    @ColumnInfo(name = "fecha")
    public String fecha;
    @ColumnInfo(name = "tarea")
    public String tarea ;

    public TareaEntity(int idUsuario, String titulo, String fecha, String tarea) {
        this.idUsuario = idUsuario;
        this.titulo = titulo;
        this.fecha = fecha;
        this.tarea = tarea;
    }
}
