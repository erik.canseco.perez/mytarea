package com.example.mytarea.db;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.mytarea.db.Entity.TareaEntity;
import com.example.mytarea.db.Entity.UsuarioEntity;
import com.example.mytarea.db.dao.TareaDao;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {UsuarioEntity.class, TareaEntity.class},version = 1)
public abstract class TareaRoomDatabase extends RoomDatabase {

    public abstract TareaDao tareaDao();
    private  static volatile TareaRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public  static TareaRoomDatabase getDatabase(final Context context){
        if(INSTANCE == null){
            synchronized (TareaRoomDatabase.class){
                if (INSTANCE==null){
                    INSTANCE= Room.databaseBuilder(context.getApplicationContext(),
                            TareaRoomDatabase.class,"tarea_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }

}
