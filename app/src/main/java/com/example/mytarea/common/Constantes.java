package com.example.mytarea.common;

public class Constantes {
    public static final String PREF_ID_USER = "PREF_ID_USER";
    public static final String PREF_USER = "PREF_USER";
    public static final String ARG_TAREA_ID = "TAREA_ID";
}
